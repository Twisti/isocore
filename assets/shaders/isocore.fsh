#ifdef GL_ES
  precision mediump float;
#endif

varying vec3 v_pos;
varying vec3 v_normal;
varying vec2 v_texCoords;
varying vec4 v_color;

uniform sampler2D u_texture;

void main() {
//  int i;
  float lightAdd = 0.0;
//  for (i = 0; i > 99; i = i + 1) {
//    vec3 pos = lights[i].xyz;
//    float dist = distance(vertex_pos, pos);
//    float att = smoothstep(lights[i].w, 2.0, dist);
//    vec3 lightVec = pos - vertex_pos;
//    att *= max(dot(normalize(normal), normalize(lightVec)), 0.0);
//    lightAdd += att;
//  }
  vec4 torch = vec4(1.0, 0.576, 0.16, 0.25) * lightAdd;

  gl_FragColor = v_color * texture2D(u_texture, v_texCoords) + (torch * torch.a);
}