attribute vec3 a_position;
attribute vec3 a_normal;
attribute vec4 a_color;
attribute vec2 a_tex;
																
uniform vec3 u_dif_v0;
uniform vec4 u_dif_c0;
uniform vec3 u_dif_v1;
uniform vec4 u_dif_c1;
uniform mat4 u_projTrans;

varying vec3 v_pos;
varying vec3 v_normal;
varying vec2 v_texCoords;
varying vec4 v_color;

void main() {
  vec3 diffuse0 = (u_dif_c0.rgb) * max(dot(a_normal, u_dif_v0), 0.0);
  vec3 diffuse1 = (u_dif_c1.rgb) * max(dot(a_normal, u_dif_v1), 0.0);
  v_color = a_color * vec4(diffuse0.xyz + diffuse1.xyz, 1.0);
  v_color += a_color * 0.2;

  v_texCoords = a_tex;
  v_normal = a_normal;
  v_pos = a_position.xyz;

  gl_Position = u_projTrans * vec4(a_position.xyz, 1.0);
}