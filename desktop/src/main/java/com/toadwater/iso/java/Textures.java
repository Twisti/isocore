package com.toadwater.iso.java;

import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.tools.texturepacker.TexturePacker;
import com.badlogic.gdx.tools.texturepacker.TexturePacker.Settings;

public class Textures {
	public static void main(String[] args) {
		Settings settings = new Settings();
		settings.maxWidth = 512;
		settings.maxHeight = 512;
		settings.filterMin = TextureFilter.Nearest;
		settings.filterMag = TextureFilter.Nearest;
		settings.paddingX = 4;
		settings.paddingY = 4;
		settings.edgePadding = true;
		settings.duplicatePadding = true;
		TexturePacker.process(settings, "../assets-raw", "../assets", "textures");
	}
}
