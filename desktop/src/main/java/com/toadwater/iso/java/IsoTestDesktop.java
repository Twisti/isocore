package com.toadwater.iso.java;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.toadwater.iso.core.IsoTest;

public class IsoTestDesktop {
	public static void main(String[] args) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = 1000;
		config.height = 600;
		config.vSyncEnabled = false;
		config.samples = 4;
		config.foregroundFPS = 0;
		new LwjglApplication(new IsoTest(), config);
	}
}
