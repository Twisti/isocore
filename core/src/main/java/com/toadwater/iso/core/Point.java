package com.toadwater.iso.core;

import com.badlogic.gdx.math.Vector3;

import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@RequiredArgsConstructor
@EqualsAndHashCode
@ToString
public class Point {
	public Point(Vector3 p) {
		this((int) p.x, (int) p.y, (int) p.z);
	}

	public final int	x, y, z;
}
