package com.toadwater.iso.core.noise;

/** Use to generate smooth perlin noise. */
public final class Perlin {
	private float interpolate(float a, float b, float x) {
		double f = (1 - Math.cos(x * Math.PI)) * .5f;
		return (float) ((a * (1 - f)) + (b * f));
	}

	private float interpolatedNoise(float x, float y) {
		float fractional_X = x % 1;
		int integer_X = (int) (x - fractional_X);

		float fractional_Y = y % 1;
		int integer_Y = (int) (y - fractional_Y);

		float v1 = smoothNoise(integer_X, integer_Y);
		float v2 = smoothNoise(integer_X + 1, integer_Y);
		float v3 = smoothNoise(integer_X, integer_Y + 1);
		float v4 = smoothNoise(integer_X + 1, integer_Y + 1);

		float i1 = interpolate(v1, v2, fractional_X);
		float i2 = interpolate(v3, v4, fractional_X);

		return interpolate(i1, i2, fractional_Y);
	}

	public float noise(int x, int y) {
		x += 1000000;
		y += 1000000;

		float total = 0;
		float p = 1 / 8f;
		float n = 4;

		for (int i = 0; i <= n; i++) {
			float frequency = (float) (Math.pow(2, i) / 16f);
			double amplitude = Math.pow(p, i);

			total += interpolatedNoise(x * frequency, y * frequency) * amplitude;

		}

		return total;
	}

	private float rawNoise(long x, long y) {
		long n = (x * 331) + (y * 337); // add your seed on this line.
		n = (n << 13) ^ n;
		long nn = ((n * ((n * n * 41333) + 53307781)) + 1376312589);
		nn = nn & 0x7fffffff;
		return (float) (((1.0 - (nn / 1073741824.0)) + 1) / 2.0);
	}

	private float smoothNoise(int x, int y) {
		float corners = (rawNoise(x - 1, y - 1) + rawNoise(x + 1, y - 1) + rawNoise(x - 1, y + 1) + rawNoise(x + 1,
				y + 1)) / 16f;
		float sides = (rawNoise(x - 1, y) + rawNoise(x + 1, y) + rawNoise(x, y - 1) + rawNoise(x, y + 1)) / 8f;
		float center = rawNoise(x, y) / 4f;
		return corners + sides + center;
	}
}
