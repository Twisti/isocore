package com.toadwater.iso.core.map;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.toadwater.iso.core.Point;
import com.toadwater.iso.core.shaders.VoxelShader;

@RequiredArgsConstructor
@EqualsAndHashCode (of = { "offset" })
public class Chunk {
	private final Point		offset;

	@Getter
	private boolean			dirty		= true;

	private final ChunkMesh	chunkMesh	= new ChunkMesh();

	@Getter
	private final byte[]	types		= new byte[WorldMap.CHUNK_SIZE * WorldMap.CHUNK_SIZE * WorldMap.CHUNK_SIZE];

	public void flush(VoxelShader vs) {
		chunkMesh.flush(vs);
	}

	public byte get(int x, int y, int z) {
		return types[(y * WorldMap.CHUNK_SIZE * WorldMap.CHUNK_SIZE) + (z * WorldMap.CHUNK_SIZE) + x];
	}

	public void rebuild(AtlasRegion texture, WorldMap wm) {
		chunkMesh.rebuild(types, offset, texture, wm);
		dirty = false;
	}

	public void set(int x, int y, int z, byte type) {
		types[(y * WorldMap.CHUNK_SIZE * WorldMap.CHUNK_SIZE) + (z * WorldMap.CHUNK_SIZE) + x] = type;
		dirty = true;
	}
}
