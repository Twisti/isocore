package com.toadwater.iso.core.map;

import java.util.Random;

import lombok.RequiredArgsConstructor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.VertexAttributes.Usage;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.NumberUtils;
import com.toadwater.iso.core.Point;
import com.toadwater.iso.core.shaders.VoxelShader;

public class ChunkMesh {
	@RequiredArgsConstructor
	public enum Geometry {
		FRONT(new Point(0, 0, 1), new Point(1, 0, 1), new Point(1, 1, 1), new Point(0, 1, 1), new Vector3(0, 0, 1)),
		BACK(new Point(1, 0, 0), new Point(0, 0, 0), new Point(0, 1, 0), new Point(1, 1, 0), new Vector3(0, 0, -1)),
		UP(new Point(0, 1, 1), new Point(1, 1, 1), new Point(1, 1, 0), new Point(0, 1, 0), new Vector3(0, 1, 0)),
		DOWN(new Point(0, 0, 1), new Point(0, 0, 0), new Point(1, 0, 0), new Point(1, 0, 1), new Vector3(0, -1, 0)),
		LEFT(new Point(0, 0, 0), new Point(0, 0, 1), new Point(0, 1, 1), new Point(0, 1, 0), new Vector3(-1, 0, 0)),
		RIGHT(new Point(1, 0, 1), new Point(1, 0, 0), new Point(1, 1, 0), new Point(1, 1, 1), new Vector3(1, 0, 0));

		public final Point		p1, p2, p3, p4;
		public final Vector3	n;
	}

	public static final int			POSITION_COMPONENTS	= 3;
	public static final int			NORMAL_COMPONENTS	= 3;
	public static final int			TEX_COMPONENTS		= 2;
	public static final int			COLOR_COMPONENTS	= 1;
	public static final int			NUM_COMPONENTS		= POSITION_COMPONENTS + NORMAL_COMPONENTS + TEX_COMPONENTS
			+ COLOR_COMPONENTS;

	public static final int			MAX_BLOCKS			= WorldMap.CHUNK_SIZE * WorldMap.CHUNK_SIZE
			* WorldMap.CHUNK_SIZE;				// for testing
			public static final int			MAX_VERTS			= MAX_BLOCKS * 4 * 6;
			public static final int			MAX_INDICES			= MAX_BLOCKS * 6 * 6;

			// Static temporary arrays
			private static final float[]	VERTICES			= new float[MAX_VERTS * NUM_COMPONENTS];
			private static final short[]	INDICES				= new short[MAX_INDICES];
			private static int				IDX_IDX				= 0;
			private static int				IDX_VERT			= 0;

			protected static ShaderProgram createMeshShader() {
				ShaderProgram.pedantic = false;
				ShaderProgram shader = new ShaderProgram(Gdx.files.internal("shaders/isocore.vsh").readString(), Gdx.files
						.internal("shaders/isocore.fsh").readString());
				String log = shader.getLog();
				if (!shader.isCompiled())
					throw new GdxRuntimeException(log);
				if ((log != null) && (log.length() != 0)) {
					System.out.println("Shader Log: " + log);
				}
				return shader;
			}

			// Instance stuff
			private Mesh				mesh;
			private int					indices;

			public static final Random	random	= new Random(1);

			public static int			sides	= 0;

			private void addGeometry(Geometry g, int x, int y, int z, float c1, float c2, AtlasRegion texture) {
				addGeometry(g, x, y, z, c1, c1, c2, c2, texture);
			}

			private void addGeometry(Geometry g, int x, int y, int z, float c1, float c2, float c3, float c4,
					AtlasRegion texture) {
				sides++;
				short idxOffset = (short) (IDX_VERT / NUM_COMPONENTS);

				VERTICES[IDX_VERT++] = x + g.p1.x;
				VERTICES[IDX_VERT++] = y + g.p1.y;
				VERTICES[IDX_VERT++] = z + g.p1.z;
				VERTICES[IDX_VERT++] = g.n.x;
				VERTICES[IDX_VERT++] = g.n.y;
				VERTICES[IDX_VERT++] = g.n.z;
				VERTICES[IDX_VERT++] = texture.getU();
				VERTICES[IDX_VERT++] = texture.getV2();
				VERTICES[IDX_VERT++] = c3;

				VERTICES[IDX_VERT++] = x + g.p2.x;
				VERTICES[IDX_VERT++] = y + g.p2.y;
				VERTICES[IDX_VERT++] = z + g.p2.z;
				VERTICES[IDX_VERT++] = g.n.x;
				VERTICES[IDX_VERT++] = g.n.y;
				VERTICES[IDX_VERT++] = g.n.z;
				VERTICES[IDX_VERT++] = texture.getU2();
				VERTICES[IDX_VERT++] = texture.getV2();
				VERTICES[IDX_VERT++] = c4;

				VERTICES[IDX_VERT++] = x + g.p3.x;
				VERTICES[IDX_VERT++] = y + g.p3.y;
				VERTICES[IDX_VERT++] = z + g.p3.z;
				VERTICES[IDX_VERT++] = g.n.x;
				VERTICES[IDX_VERT++] = g.n.y;
				VERTICES[IDX_VERT++] = g.n.z;
				VERTICES[IDX_VERT++] = texture.getU2();
				VERTICES[IDX_VERT++] = texture.getV();
				VERTICES[IDX_VERT++] = c1;

				VERTICES[IDX_VERT++] = x + g.p4.x;
				VERTICES[IDX_VERT++] = y + g.p4.y;
				VERTICES[IDX_VERT++] = z + g.p4.z;
				VERTICES[IDX_VERT++] = g.n.x;
				VERTICES[IDX_VERT++] = g.n.y;
				VERTICES[IDX_VERT++] = g.n.z;
				VERTICES[IDX_VERT++] = texture.getU();
				VERTICES[IDX_VERT++] = texture.getV();
				VERTICES[IDX_VERT++] = c2;

				INDICES[IDX_IDX++] = (short) (idxOffset + 0);
				INDICES[IDX_IDX++] = (short) (idxOffset + 1);
				INDICES[IDX_IDX++] = (short) (idxOffset + 2);

				INDICES[IDX_IDX++] = (short) (idxOffset + 2);
				INDICES[IDX_IDX++] = (short) (idxOffset + 3);
				INDICES[IDX_IDX++] = (short) (idxOffset + 0);
			}

			public void flush(VoxelShader vs) {
				if (indices == 0)
					return;

				vs.render(mesh, indices);
			}

			public void rebuild(byte[] types, Point offset, AtlasRegion texture, WorldMap wm) {
				sides -= indices / 3;
				IDX_VERT = 0;
				IDX_IDX = 0;

				for (int i = 0; i < types.length; i++) {
					if (types[i] != 0) {
						int x = i % WorldMap.CHUNK_SIZE;
						int y = i / (WorldMap.CHUNK_SIZE * WorldMap.CHUNK_SIZE);
						int z = (i / WorldMap.CHUNK_SIZE) % WorldMap.CHUNK_SIZE;
						int x1 = x + (offset.x * WorldMap.CHUNK_SIZE) + (offset.x < 0 ? 1 : 0);
						int y1 = y + (offset.y * WorldMap.CHUNK_SIZE) + (offset.y < 0 ? 1 : 0);
						int z1 = z + (offset.z * WorldMap.CHUNK_SIZE) + (offset.z < 0 ? 1 : 0);
						float c1 = Color.WHITE.toFloatBits();
						float c2 = Color.GRAY.toFloatBits();

						if ((y == (WorldMap.CHUNK_SIZE - 1)) || (types[i + (WorldMap.CHUNK_SIZE * WorldMap.CHUNK_SIZE)] == 0)) {
							float uc4 = vAO(x1, y1, z1, 1, 1, wm);
							float uc3 = vAO(x1, y1, z1, -1, 1, wm);
							float uc2 = vAO(x1, y1, z1, -1, -1, wm);
							float uc1 = vAO(x1, y1, z1, 1, -1, wm);
							addGeometry(Geometry.UP, x1, y1, z1, uc1, uc2, uc3, uc4, texture);
						}
						if ((y == 0) || (types[i - (WorldMap.CHUNK_SIZE * WorldMap.CHUNK_SIZE)] == 0)) {
							addGeometry(Geometry.DOWN, x1, y1, z1, c1, c1, texture);
						}
						if ((z == (WorldMap.CHUNK_SIZE - 1)) || (types[i + WorldMap.CHUNK_SIZE] == 0)) {
							addGeometry(Geometry.FRONT, x1, y1, z1, c1, wm.get(x1, y1 - 1, z1 + 1) != 0 ? c2 : c1, texture);
						}
						if ((z == 0) || (types[i - WorldMap.CHUNK_SIZE] == 0)) {
							addGeometry(Geometry.BACK, x1, y1, z1, c1, wm.get(x1, y1 - 1, z1 - 1) != 0 ? c2 : c1, texture);
						}
						if ((x == 0) || (types[i - 1] == 0)) {
							addGeometry(Geometry.LEFT, x1, y1, z1, c1, wm.get(x1 - 1, y1 - 1, z1) != 0 ? c2 : c1, texture);
						}
						if ((x == (WorldMap.CHUNK_SIZE - 1)) || (types[i + 1] == 0)) {
							addGeometry(Geometry.RIGHT, x1, y1, z1, c1, wm.get(x1 + 1, y1 - 1, z1) != 0 ? c2 : c1, texture);
						}
					}
				}

				mesh = new Mesh(true, IDX_VERT / NUM_COMPONENTS, IDX_IDX, new VertexAttribute(Usage.Position,
						POSITION_COMPONENTS, "a_position"), new VertexAttribute(Usage.Normal, NORMAL_COMPONENTS, "a_normal"),
						new VertexAttribute(Usage.TextureCoordinates, 2, "a_tex"), new VertexAttribute(Usage.ColorPacked, 4,
								"a_color"));
				mesh.setVertices(VERTICES, 0, IDX_VERT);
				mesh.setIndices(INDICES, 0, IDX_IDX);
				indices = IDX_IDX;
			}

			public void singleCube(Point p, AtlasRegion texture, WorldMap wm) {
				sides -= indices / 3;
				IDX_VERT = 0;
				IDX_IDX = 0;
				int x1 = p.x;
				int y1 = p.y;
				int z1 = p.z;

				float uc4 = vAO(x1, y1, z1, 1, 1, wm);
				float uc3 = vAO(x1, y1, z1, -1, 1, wm);
				float uc2 = vAO(x1, y1, z1, -1, -1, wm);
				float uc1 = vAO(x1, y1, z1, 1, -1, wm);
				float c1 = Color.WHITE.toFloatBits();
				float c2 = Color.GRAY.toFloatBits();

				addGeometry(Geometry.UP, x1, y1, z1, uc1, uc2, uc3, uc4, texture);
				addGeometry(Geometry.DOWN, x1, y1, z1, c1, c1, texture);
				addGeometry(Geometry.FRONT, x1, y1, z1, c1, wm.get(x1, y1 - 1, z1 + 1) != 0 ? c2 : c1, texture);
				addGeometry(Geometry.BACK, x1, y1, z1, c1, wm.get(x1, y1 - 1, z1 - 1) != 0 ? c2 : c1, texture);
				addGeometry(Geometry.LEFT, x1, y1, z1, c1, wm.get(x1 - 1, y1 - 1, z1) != 0 ? c2 : c1, texture);
				addGeometry(Geometry.RIGHT, x1, y1, z1, c1, wm.get(x1 + 1, y1 - 1, z1) != 0 ? c2 : c1, texture);

				mesh = new Mesh(true, IDX_VERT / NUM_COMPONENTS, IDX_IDX, new VertexAttribute(Usage.Position,
						POSITION_COMPONENTS, "a_position"), new VertexAttribute(Usage.Normal, NORMAL_COMPONENTS, "a_normal"),
						new VertexAttribute(Usage.TextureCoordinates, 2, "a_tex"), new VertexAttribute(Usage.ColorPacked, 4,
								"a_color"));
				mesh.setVertices(VERTICES, 0, IDX_VERT);
				mesh.setIndices(INDICES, 0, IDX_IDX);
				indices = IDX_IDX;
			}

			/** Returns the ambient occlusion color for a vertex */
			private float vAO(int x, int y, int z, int u, int v, WorldMap wm) {
				float vaof = (vAOFactor(x, y, z, u, v, wm) / 4f) + 0.25f;
				int color = ((255 * 1) << 24) | ((int) (255 * vaof) << 16) | ((int) (255 * vaof) << 8) | ((int) (255 * vaof));
				return NumberUtils.intToFloatColor(color);
			}

			private float vAOFactor(int x, int y, int z, int u, int v, WorldMap wm) {
				byte side1 = wm.get(x + u, y + 1, z);
				byte side2 = wm.get(x, y + 1, z + v);
				byte corner = wm.get(x + u, y + 1, z + v);
				if ((side1 != 0) && (side2 != 0))
					return 0;

				return 3 - (side1 + side2 + corner);
			}
}
