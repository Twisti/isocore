package com.toadwater.iso.core;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.TextureAtlasLoader;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.graphics.g3d.utils.AnimationController;
import com.badlogic.gdx.graphics.g3d.utils.AnimationController.AnimationDesc;
import com.badlogic.gdx.graphics.g3d.utils.AnimationController.AnimationListener;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.Ray;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.toadwater.iso.core.map.Chunk;
import com.toadwater.iso.core.map.ChunkMesh;
import com.toadwater.iso.core.map.WorldMap;
import com.toadwater.iso.core.noise.Perlin;
import com.toadwater.iso.core.shaders.VoxelShader;

public class IsoTest implements ApplicationListener, InputProcessor {
	Camera						cam;
	private int					_touchedX;
	private int					_touchedY;
	private float				_rotX;
	private float				_rotZ;
	private float				_rotY;
	private Viewport			viewport;
	private final Vector3		tmp			= new Vector3();
	WorldMap					wm			= new WorldMap();

	int							xMovement	= 0;
	int							yMovement	= 0;
	int							zMovement	= 0;

	private AtlasRegion			rock;
	private ModelBatch			modelBatch;
	private AssetManager		assets;
	private ModelInstance		teensy;
	private Environment			environment;
	private AnimationController	animationController;
	Vector3						impulse		= new Vector3(0, 0, 0);

	int							lastZ		= 10;
	int							lastY		= 10;
	private float				zAngle;
	private float				zSpeed		= 15;
	private float				PI2			= (float) (Math.PI * 2);

	float						fakeZ		= 0;

	boolean						jump		= false;
	private Point				mouseover;
	private ChunkMesh			cm;
	private AtlasRegion			rockSelected;
	private VoxelShader			voxelShader;

	@Override
	public void create() {
		ScreenConsole.initialise();

		cam = new PerspectiveCamera(65, 25, 25);
		viewport = new ExtendViewport(25, 25, cam);
		cam.position.set(-5.5f, -36.8f, -5.3f);
		cam.direction.set(0.64f, -0.59f, 0.50f);
		cam.up.set(0, 1, 0);
		cam.near = 1f;
		cam.far = 1000;
		cam.update();
		rotateY(0);

		voxelShader = VoxelShader.initialize();
		voxelShader.setCamera(cam);
		voxelShader.addDirectionalLight(new Vector3(-5f, -2f, -1f), new Color(0.7f, 0.75f, 1f, 0.4f));
		voxelShader.addDirectionalLight(new Vector3(-2f, 6, 0.1f), new Color(1f, 0.95f, 0.9f, 1f));

		Perlin perlin = new Perlin();
		int dims = 100;
		for (int x = -dims; x < dims; x++) {
			for (int z = -dims; z < dims; z++) {
				float localNoise = perlin.noise(x, z);
				for (int y = -100; y < 100; y++) {
					if ((y + 100) < (localNoise * 100)) {
						wm.set(x, y, z, 1);
					}
				}
			}
		}

		TextureAtlasLoader.TextureAtlasParameter param = new TextureAtlasLoader.TextureAtlasParameter();
		modelBatch = new ModelBatch();
		assets = new AssetManager();
		assets.load("textures.atlas", TextureAtlas.class, param);
		assets.load("fatty.g3db", Model.class);
		assets.finishLoading();

		TextureAtlas atlas;
		atlas = assets.get("textures.atlas", TextureAtlas.class);
		rock = atlas.findRegion("rock");
		rockSelected = atlas.findRegion("rockline2");
		Model model = assets.get("fatty.g3db", Model.class);
		teensy = new ModelInstance(model);
		animationController = new AnimationController(teensy);

		teensy.transform.rotate(0, 1, 0, 180);
		animationController.animate("walk", -1, 2f, null, 0f);
		for (int y = -100; y < 100; y++) {
			if (wm.get(0, y, 0) == 0) {
				teensy.transform.trn(0.5f, y, 0.5f);
				lastY = y;
				y = 999;
			}
		}

		environment = new Environment();
		environment.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.7f, 0.7f, 0.7f, 1f));
		environment.add(new DirectionalLight().set(0.8f, 0.8f, 0.8f, 0.3f, -1.8f, 0.5f));
		Gdx.input.setInputProcessor(this);
	}

	@Override
	public void dispose() {}

	@Override
	public boolean keyDown(int keycode) {
		updateMovement();

		return true;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		updateMovement();

		return true;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		Ray pickray = cam.getPickRay(screenX, screenY).cpy();
		FingerPicker fp = new FingerPicker(pickray, wm);
		mouseover = fp.pick7();
		return true;
	}

	@Override
	public void pause() {}

	@Override
	public void render() {
		//		rotateY((Gdx.graphics.getDeltaTime() * 15f));
		if (jump) {
			fakeZ += Gdx.graphics.getDeltaTime() * 2f;
		}
		teensy.transform.getTranslation(tmp);
		int lastLastY = lastY;
		//		lastY = (int) ((perlinNoise(tmp.x, tmp.z - 1.8f) * 100) - 100);
		lastY = ((50) * 100) - 100;
		if (tmp.y > lastY) {
			//			impulse.y -= Gdx.graphics.getDeltaTime() * 15f;
		} else {
			//			impulse.y = 0;
		}
		if (tmp.z < lastZ) {
			lastZ = (int) tmp.z;
			if (lastZ < -10) {
				//				teensy.transform.setTranslation(0.5f, (int) ((perlinNoise(tmp.x, 1) * 100) - 97), 10);
				lastZ = 10;
				impulse.set(0, 0, -0f);
			}
		}
		if (lastLastY != lastY) {
			jump = true;
			animationController.animate("jump", 1, 2f, new AnimationListener() {
				@Override
				public void onEnd(AnimationDesc animation) {
					teensy.transform.trn(0, -1, -1.4f);
					impulse.z = -1;
					fakeZ = 0;
					jump = false;
				}

				@Override
				public void onLoop(AnimationDesc animation) {}
			}, 0.2f);
			impulse.z = 0;
			animationController.queue("walk", 0.18f, -1, -1, 2f, null, 0f);
			//			impulse.y = 3;
		}

		teensy.transform.trn(tmp.set(impulse).scl(Gdx.graphics.getDeltaTime() * 2.5f));

		tmp.set(cam.direction).crs(cam.up).nor();
		cam.position.add(tmp.scl(Gdx.graphics.getDeltaTime() * xMovement * -100f));
		tmp.set(cam.up).nor();
		cam.position.add(tmp.scl(Gdx.graphics.getDeltaTime() * yMovement * 100f));
		tmp.set(cam.direction).nor();
		cam.position.add(tmp.scl(Gdx.graphics.getDeltaTime() * zMovement * 100f));
		cam.update();
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

		animationController.update(Gdx.graphics.getDeltaTime());
		modelBatch.begin(cam);
		//		modelBatch.render(teensy, environment);
		modelBatch.end();

		zAngle += Gdx.graphics.getDeltaTime() * zSpeed;
		while (zAngle > PI2) {
			zAngle -= PI2;
		}
		//		float lightSize = (4.75f + (0.25f * (float) Math.sin(zAngle)) + (.2f *  MathUtils.random()));
		float lightSize = (3.75f + (0.25f * (float) Math.sin(zAngle)) + (.2f * 1));
		voxelShader.begin();
		rock.getTexture().bind();
		Gdx.gl.glEnable(GL20.GL_DEPTH_TEST);
		Gdx.gl.glDepthMask(true);
		Gdx.gl.glEnable(GL20.GL_CULL_FACE);
		Gdx.gl.glCullFace(GL20.GL_BACK);

		int rebuilt = 0;

		for (Chunk c : wm.getChunks().values()) {
			if (c.isDirty()) {
				c.rebuild(rock, wm);
				rebuilt++;
			}
			c.flush(voxelShader);

			if (rebuilt >= 16) {
				break;
			}
		}

		if (mouseover != null) {
			Gdx.gl.glDepthFunc(GL20.GL_EQUAL | GL20.GL_LESS);
			cm = new ChunkMesh();
			cm.singleCube(mouseover, rockSelected, wm);
			cm.flush(voxelShader);
			Gdx.gl.glDepthFunc(GL20.GL_LESS);
		}

		voxelShader.end();

		Gdx.gl.glDisable(GL20.GL_CULL_FACE);

		ScreenConsole.log("chunk size", WorldMap.CHUNK_SIZE + "x" + WorldMap.CHUNK_SIZE + "x" + WorldMap.CHUNK_SIZE);
		ScreenConsole.log("chunks", "" + wm.getChunks().size());
		ScreenConsole.log("triangles", "" + ChunkMesh.sides);
		ScreenConsole.log("fps", "" + Gdx.graphics.getFramesPerSecond());
		ScreenConsole.log("cam pos",
				String.format("% 3.2f, % 3.2f, % 3.2f", cam.position.x, cam.position.y, cam.position.z));
		ScreenConsole.log("cam aim",
				String.format("% 3.2f, % 3.2f, % 3.2f", cam.direction.x, cam.direction.y, cam.direction.z));
		ScreenConsole.render();
	}

	@Override
	public void resize(int width, int height) {
		viewport.update(width, height);
		ScreenConsole.resize(width, height);
	}

	@Override
	public void resume() {}

	private void rotateX(float degree) {
		_rotX = (_rotX + degree) % 360; // Save the total x-rotation
		Vector3 lookAtPoint = new Vector3();

		float orbitRadius = lookAtPoint.dst(cam.position);

		//				cam.position.set(lookAtPoint);
		tmp.set(cam.direction).crs(cam.up);
		cam.direction.rotate(tmp, degree);

		Vector3 orbitReturnVector = new Vector3();
		orbitReturnVector.set(tmp.set(cam.direction).nor().scl(-orbitRadius));

		//				cam.translate(orbitReturnVector.x, orbitReturnVector.y, orbitReturnVector.z);
	}

	private void rotateY(float degree) {
		_rotY = (_rotY + degree) % 360;

		Vector3 lookAtPoint = new Vector3();

		float orbitRadius = lookAtPoint.dst(cam.position);

		//		cam.position.set(lookAtPoint);
		//		tmp.set(cam.direction).crs(cam.up);
		cam.direction.rotate(cam.up, degree);

		Vector3 orbitReturnVector = new Vector3();
		orbitReturnVector.set(tmp.set(cam.direction).nor().scl(-orbitRadius));

		//		cam.translate(orbitReturnVector.x, orbitReturnVector.y, orbitReturnVector.z);
	}

	private void rotateZ(float degree) {
		//		_rotZ = (_rotZ + degree) % 360;
		//		Vector3 lookAtPoint = new Vector3();
		//
		//		float orbitRadius = lookAtPoint.dst(cam.position);
		//
		//		cam.position.set(lookAtPoint);
		//		cam.direction.rotate(degree, 0, 0, 1);
		//
		//		Vector3 orbitReturnVector = new Vector3();
		//		orbitReturnVector.set(tmp.set(cam.direction).nor().scl(-orbitRadius));
		//
		//		cam.translate(orbitReturnVector.x, orbitReturnVector.y, orbitReturnVector.z);
	}

	@Override
	public boolean scrolled(int amount) {
		cam.position.sub(tmp.set(cam.direction).nor().scl(10).scl(amount));
		return true;
	}

	@Override
	public boolean touchDown(int x, int y, int pointer, int button) {
		_touchedX = x;
		_touchedY = y;

		return true;
	}

	@Override
	public boolean touchDragged(int x, int y, int pointer) {
		if (Gdx.input.isButtonPressed(2)) {
			int diffX = _touchedX - x;
			int diffY = _touchedY - y;

			float currRotX = _rotX;
			float currRotY = _rotY;

			rotateX(-currRotX); // undo X-rotation
			rotateY(diffX); // rotate Y
			rotateX(currRotX); // reset X-rotation

			rotateY(-currRotY);
			rotateX(diffY);
			rotateY(currRotY);

			_touchedX = x;
			_touchedY = y;

			return true;
		} else return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		if (mouseover != null) {
			if (button == 1) {
				wm.set(mouseover.x, mouseover.y, mouseover.z, 0);
			} else if (button == 0) {
				wm.set(mouseover.x, mouseover.y + 1, mouseover.z, 1);
			}
			mouseover = null;
			mouseMoved(screenX, screenY);
			return true;
		}
		return false;
	}

	private void updateMovement() {
		xMovement = 0;
		yMovement = 0;
		zMovement = 0;

		if (Gdx.input.isKeyPressed(Input.Keys.A)) {
			xMovement = 1;
		}
		if (Gdx.input.isKeyPressed(Input.Keys.D)) {
			xMovement = -1;
		}
		if (Gdx.input.isKeyPressed(Input.Keys.W)) {
			zMovement = 1;
		}
		if (Gdx.input.isKeyPressed(Input.Keys.S)) {
			zMovement = -1;
		}
		if (Gdx.input.isKeyPressed(Input.Keys.SPACE)) {
			yMovement = 1;
		}
		if (Gdx.input.isKeyPressed(Input.Keys.CONTROL_LEFT)) {
			yMovement = -1;
		}
	}
}
