package com.toadwater.iso.core;

import lombok.AllArgsConstructor;

import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.Ray;
import com.toadwater.iso.core.map.WorldMap;

@AllArgsConstructor
public class FingerPicker {
	private Ray			pickray;
	private WorldMap	wm;

	public Point pick7() {
		Vector3 pos = pickray.origin;
		Vector3 dir = pickray.direction;

		int steps = 0;
		boolean found = false;
		Point p = null;
		while (!found) {

			float xLength = (float) (((dir.x > 0 ? Math.floor(pos.x) + 1 : Math.ceil(pos.x) - 1) - pos.x) / dir.x);
			float yLength = (float) (((dir.y > 0 ? Math.floor(pos.y) + 1 : Math.ceil(pos.y) - 1) - pos.y) / dir.y);
			float zLength = (float) (((dir.z > 0 ? Math.floor(pos.z) + 1 : Math.ceil(pos.z) - 1) - pos.z) / dir.z);

			if (xLength < yLength) {
				if (xLength < zLength) {
					pos.add(dir.scl(xLength));
				} else {
					pos.add(dir.scl(zLength));
				}
			} else {
				if (yLength < zLength) {
					pos.add(dir.scl(yLength));
				} else {
					pos.add(dir.scl(zLength));
				}
			}

			pos.add(dir.scl(0.001f));

			p = new Point((int) Math.ceil(pos.x) - 1, (int) Math.ceil(pos.y) - 1, (int) Math.ceil(pos.z) - 1);
			if (wm.get(p) != 0) {
				found = true;
			}

			if (steps++ > 100)
				return null;
		}

		return p;
	}
}
