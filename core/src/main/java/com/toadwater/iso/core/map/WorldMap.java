package com.toadwater.iso.core.map;

import java.util.HashMap;
import java.util.Map;

import com.toadwater.iso.core.Point;

import lombok.Getter;

public class WorldMap {
	/** Chunk dimension in bits - 1 = 2, 2 = 4, 3 = 8, 4 = 16 and so on. */
	private static final int	CHUNK_BITS	= 5;
	public static final int		CHUNK_SIZE	= 1 << CHUNK_BITS;

	@Getter
	private Map<Long, Chunk>	chunks		= new HashMap<Long, Chunk>();

	public byte get(int x, int y, int z) {
		int x2 = (x % CHUNK_SIZE) + (x >= 0 ? 0 : (CHUNK_SIZE - 1));
		int y2 = (y % CHUNK_SIZE) + (y >= 0 ? 0 : (CHUNK_SIZE - 1));
		int z2 = (z % CHUNK_SIZE) + (z >= 0 ? 0 : (CHUNK_SIZE - 1));
		Chunk chunk = getChunk(x, y, z, false);
		return chunk == null ? 0 : chunk.get(x2, y2, z2);
	}

	public byte get(Point p) {
		return get(p.x, p.y, p.z);
	}

	private Chunk getChunk(int x, int y, int z, boolean create) {
		Chunk chunk;

		int x1 = (x / CHUNK_SIZE) - (x < 0 ? 1 : 0);
		int y1 = (y / CHUNK_SIZE) - (y < 0 ? 1 : 0);
		int z1 = (z / CHUNK_SIZE) - (z < 0 ? 1 : 0);
		long coords = 0;
		coords |= (x1 & 0xfffff) | ((x1 & Integer.MIN_VALUE) >>> 11);
		coords |= (long) ((y1 & 0xfffff) | ((y1 & Integer.MIN_VALUE) >>> 11)) << 21;
		coords |= (long) ((z1 & 0xfffff) | ((z1 & Integer.MIN_VALUE) >>> 11)) << 42;

		chunk = chunks.get(coords);

		if ((chunk == null) && create) {
			Point point = new Point(x1, y1, z1);
			chunk = new Chunk(point);
			chunks.put(coords, chunk);
		}

		return chunk;
	}

	public void set(int x, int y, int z, int type) {
		int x2 = (x % CHUNK_SIZE) + (x >= 0 ? 0 : (CHUNK_SIZE - 1));
		int y2 = (y % CHUNK_SIZE) + (y >= 0 ? 0 : (CHUNK_SIZE - 1));
		int z2 = (z % CHUNK_SIZE) + (z >= 0 ? 0 : (CHUNK_SIZE - 1));
		Chunk chunk = getChunk(x, y, z, true);
		chunk.set(x2, y2, z2, (byte) type);
	}
}
