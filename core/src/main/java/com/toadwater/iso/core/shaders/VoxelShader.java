package com.toadwater.iso.core.shaders;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.GdxRuntimeException;

@RequiredArgsConstructor (access = AccessLevel.PRIVATE)
public final class VoxelShader {
	public static VoxelShader initialize() {
		ShaderProgram.pedantic = true;
		ShaderProgram shader = new ShaderProgram(shader("isocore.vsh"), shader("isocore.fsh"));
		String log = shader.getLog();
		if (!shader.isCompiled())
			throw new GdxRuntimeException(log);
		if ((log != null) && (log.length() != 0)) {
			System.out.println("Shader Log: " + log);
		}
		VoxelShader vs = new VoxelShader(shader);

		return vs;
	}

	private static String shader(String name) {
		return Gdx.files.internal("shaders/" + name).readString();
	}

	private final ShaderProgram		shader;

	@Setter
	private Camera					camera;

	private Array<DirectionalLight>	directionalLights;

	public void addDirectionalLight(Vector3 direction, Color color) {
		if (directionalLights == null) {
			directionalLights = new Array<DirectionalLight>(false, 8);
		}
		directionalLights.add(new DirectionalLight().set(color.premultiplyAlpha(), direction));
	}

	public void begin() {
		shader.begin();
		shader.setUniformMatrix("u_projTrans", camera.combined);
		for (int i = 0; i < directionalLights.size; i++) {
			DirectionalLight l = directionalLights.get(i);
			shader.setUniformf("u_dif_c" + i, l.color);
			shader.setUniformf("u_dif_v" + i, l.direction);
		}
	}

	public void end() {
		shader.end();
	}

	public void render(Mesh mesh, int indices) {
		mesh.render(shader, GL20.GL_TRIANGLES, 0, indices);
	}
}
