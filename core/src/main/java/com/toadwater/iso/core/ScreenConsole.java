package com.toadwater.iso.core;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;

/**
 * Created by Scott (SudsDev on #libgdx) on 13/07/2014.
 */
public class ScreenConsole {

	protected static class LogEntry {
		public String	tag;
		public String	content;

		public LogEntry(String tag, String content) {
			this.tag = tag;
			this.content = content;
		}
	}

	private static ArrayList<LogEntry>	_entries;
	private static SpriteBatch			_batch;
	private static BitmapFont			_font;

	private static BitmapFont			_tagFont;
	private static Color				_tagColour;
	private static Color				_contentColour;

	private static Color				_shadowColour;

	private static OrthographicCamera	_camera;

	private static final float			cornerBuffer	= 10.0f;

	private static final float			lineSpacing		= 5.0f;
	private static final float			dropShadow		= 1.0f;

	public static void initialise() {
		_entries = new ArrayList<LogEntry>();

		_batch = new SpriteBatch();

		_tagColour = new Color(0.34f, 0.49f, 1.0f, 1.0f);
		_contentColour = new Color(0.9f, 0.9f, 0.9f, 1.0f);
		_shadowColour = new Color(0.0f, 0.0f, 0.0f, 0.9f);

		_camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		_camera.translate(Gdx.graphics.getWidth() / 2.0f, -Gdx.graphics.getHeight() / 2.0f);
		_camera.update();

		FreeTypeFontGenerator.FreeTypeFontParameter paramaters = new FreeTypeFontGenerator.FreeTypeFontParameter();
		paramaters.size = (int) (_camera.viewportHeight / 32);

		FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/CONTF___.ttf"));
		FreeTypeFontGenerator tagGenerator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/CONTF___.ttf"));

		_font = generator.generateFont(paramaters);
		_font.setColor(Color.WHITE);

		_tagFont = tagGenerator.generateFont(paramaters);
		_tagFont.setColor(_tagColour);
	}

	public static void log(String tag, String content) {
		_entries.add(new LogEntry(tag, content));
	}

	public static void render() {
		_batch.setProjectionMatrix(_camera.projection);
		_batch.setTransformMatrix(_camera.view);
		_batch.begin();
		{
			for (int i = 0; i < _entries.size(); i++) {
				LogEntry e = _entries.get(i);
				String tagstr = "[" + e.tag + "] ";

				_tagFont.setColor(_shadowColour);
				_tagFont.draw(_batch, tagstr, cornerBuffer + dropShadow, -cornerBuffer
						- (i * (_font.getLineHeight() + lineSpacing)) - dropShadow);

				_tagFont.setColor(_tagColour);
				_tagFont.draw(_batch, tagstr, cornerBuffer, -cornerBuffer - (i * (_font.getLineHeight() + lineSpacing)));

				_font.setColor(_shadowColour);
				_font.draw(_batch, e.content, cornerBuffer + _tagFont.getBounds(tagstr).width + dropShadow,
						-cornerBuffer - (i * (_font.getLineHeight() + lineSpacing)) - dropShadow);

				_font.setColor(_contentColour);
				_font.draw(_batch, e.content, cornerBuffer + _tagFont.getBounds(tagstr).width, -cornerBuffer
						- (i * (_font.getLineHeight() + lineSpacing)));
			}
		}
		_batch.end();

		_entries.clear();
	}

	public static void resize(int width, int height) {
		_camera = new OrthographicCamera(width, height);
		_camera.translate(width / 2.0f, -height / 2.0f);
		_camera.update();
	}
}