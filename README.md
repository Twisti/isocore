# README #

Primitive voxel / minecraftish engine skeleton that generates a cube world based off of perlin noise. Released for educational purposes, you are free to do whatever you want with the source. Just no war crimes.

Looks like this:

[video](https://www.dropbox.com/s/31xzb5vuuf2geh6/isocore.mp4?dl=0)

```
#!shell

git clone https://Twisti@bitbucket.org/Twisti/isocore.git
cd isocore
git fetch
git checkout presentation
mvn clean package -Pdesktop
java -jar desktop/target/iso-desktop-0.0.1-SNAPSHOT-jar-with-dependencies.jar

```